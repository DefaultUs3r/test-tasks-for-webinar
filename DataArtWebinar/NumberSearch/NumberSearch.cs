﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NumberSearch
{
    class NumberSearch
    {
        /// <summary>
        /// При разборе задачи была найдена закономерность,
        /// что при суммировании итераторов можно получить необходимое число
        /// или число, которое следует за ним. Для обнаружения необходимо найти
        /// итератор, который при умножении на 2 дает разницу между числом,
        /// которое ввёл пользователь и суммой итераторов
        /// </summary>
        /// <param name="number">Число введенное пользователем</param>
        /// <returns>Список членов итоговой суммы</returns>
        static List<int> GetMoves(int number)
        {
            List<int> iterations = new List<int>();
            int counter = 0;
            for (int i = 1; counter < number; i++)
            {
                iterations.Add(i);
                counter += i;
            }
            if(counter != number && (counter - number) % 2 != 0)
            {
                iterations.Add(iterations.Last() + 1);
                counter += iterations.Last();
            }

            if (counter != number && (counter - number) % 2 == 0)
            {
                int t = (counter - number) / 2;
                iterations[iterations.FindIndex(x => x == t)] *= -1;
                return iterations;
            }
            else
                return iterations;            
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Please input your number:");
            string userNumber = Console.ReadLine();
            int number = int.TryParse(userNumber, out number) ? number : 0;
            if (number > 0)
                Console.WriteLine(GetMoves(number).Count);
            else
                Console.WriteLine("Invalid input!");
        }
    }
}
