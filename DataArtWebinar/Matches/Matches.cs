﻿using System;

namespace Matches
{
    class Matches
    {        
        /// <summary>
        /// Определение размерности куба
        /// </summary>
        /// <param name="dimension">размерность</param>
        /// <param name="numberOfCubes">количество кубов</param>
        /// <param name="power">степень</param>
        /// <returns>размерность</returns>
        static int GetDimension(int dimension, int numberOfCubes, int power)
        {
            do
            {
                dimension = dimension + 1;
            } while (Math.Pow(dimension, power) < numberOfCubes);
            return dimension;
        }
        /// <summary>
        /// Расчет минимального количества спичек
        /// </summary>
        /// <param name="cubeCount">Количество спичек введённое пользователем</param>
        /// <returns>количество спичек</returns>
        static int GetAmountOfMatches(int cubeCount)
        {
            int result;
            int dimension = 0, balance = 0, plane = 0;
            //находим разрядность куба
            dimension = GetDimension(dimension, cubeCount, 3);
            
            if ((dimension * dimension * (dimension - 1)) >= cubeCount)
            {                
                if (((dimension - 1) * (dimension - 1) * dimension) >= cubeCount)
                {
                    int firstStage = dimension - 1;
                    //результату присваем прошылй куб
                    result = 8 * firstStage + 4 + 5 * firstStage * (firstStage - 1) * 2 + 
                                    3 * (firstStage - 1) * 2 + 3 * firstStage * (firstStage - 1) * (firstStage - 1) + 
                                            2 * (firstStage - 1) * (firstStage - 1);
                    //количество оставшихся кубов
                    balance = cubeCount - firstStage * firstStage * firstStage;
                    //разрядность плоскости
                    plane = GetDimension(plane, balance, 2);
                    //проверка разрядности на 1
                    if (plane == 1)
                        //увеличение результата на 8 при разрядности 1
                        result += 8;
                    else
                        //увеличение результата по формуле
                        result += 8 + (plane - 2) * 2 * 5 + 3 * ((plane - 1) * (plane - 1) - 1 - (plane - 2) * 2);
                    for (dimension = 1; (plane - 1) * (plane - 1) + dimension <= balance; dimension++)
                    {
                        if ((dimension == 1 || dimension == plane) && plane != 1)
                        {
                            result += 5;
                        }
                        else if (plane != 1)
                        {
                            result += 3;
                        }
                    }

                }
                else
                {
                    int secondStage = dimension - 1;
                    //результат по формуле+ещё достроеная на первом этапе стенка
                    result = 8 * secondStage + 4 + 5 * secondStage * (secondStage - 1) * 2 + 
                                 3 * (secondStage - 1) * 2 + 3 * secondStage * (secondStage - 1) * (secondStage - 1) + 
                                        2 * (secondStage - 1) * (secondStage - 1) + secondStage * secondStage * 3 + 5 + 2 * 2 * (secondStage - 1);
                    balance = cubeCount - secondStage * secondStage * (secondStage + 1);
                    plane = GetDimension(plane, balance, 2);
                    if (plane == 1)
                        result += 8;
                    else
                        result += 8 + (plane - 2) * 2 * 5 + 3 * ((plane - 1) * (plane - 1) - 1 - (plane - 2) * 2);
                    for (dimension = 1; ((plane - 1) * (plane - 1) + dimension) <= balance; dimension++)
                    {
                        if ((dimension == 1 || dimension == plane) && plane != 1)
                        {
                            result += 5;
                        }
                        else if (plane != 1)
                        {
                            result += 3;
                        }
                    }
                }
            }
            else
            {
                int thirdStage = dimension;
                result = 8 * (thirdStage - 1) + 4 + 5 * (thirdStage - 1) * (thirdStage - 1) * 2 + 3 * (thirdStage - 1) * 2 + 3 * (thirdStage - 1) * (thirdStage - 1) * (thirdStage - 1) + 2 * (thirdStage - 1) * (thirdStage - 1);//результат по формуле разрядности,только на высоту раную i-1
                balance = cubeCount - (thirdStage - 1) * thirdStage * thirdStage;
                plane = GetDimension(plane, balance, 2);
                if (plane == 1)
                    result += 8;
                else
                    result += 8 + (plane - 2) * 2 * 5 + 3 * ((plane - 1) * (plane - 1) - 1 - (plane - 2) * 2);
                for (dimension = 1; ((plane - 1) * (plane - 1) + dimension) <= balance; dimension++)
                {
                    if ((dimension == 1 || dimension == plane) && plane != 1)
                    {
                        result += 5;
                    }
                    else if (plane != 1)
                    {
                        result += 3;
                    }
                }
            }
            return result;
        }

        static void Main(string[] args)
        {
            int numberOfCubes = 0;
            
            Console.WriteLine("Please input your number:");
            string userNumber = Console.ReadLine();
            numberOfCubes = int.TryParse(userNumber, out numberOfCubes) ? numberOfCubes : 0;
            if (numberOfCubes > 0)
            {
                
                Console.WriteLine(GetAmountOfMatches(numberOfCubes).ToString());
            }
            else
                Console.WriteLine("Invalid input!");
            
        }
    }
}
