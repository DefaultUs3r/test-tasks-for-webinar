﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace LargerNumber
{
    class LargerNumber
    {
        /// <summary>
        /// Определяем следующее большее число
        /// на основе цифр числа введённого пользователем
        /// </summary>
        /// <param name="numbers">Список цифр числа, которое ввёл пользователь</param>
        /// <returns>Новое число в виде списка цифр</returns>
        static List<int> GetNextNumber(List<int> numbers)
        {
            const int NEXTVALUE = 1;
            
            int digitPlace = 0;
            int digitForChange = 0;
            List<int> resultList = new List<int>();
            for (int i = 0; i < numbers.Count; i++)
            {
                if ((i + NEXTVALUE) <numbers.Count && numbers[i + NEXTVALUE] < numbers[i])
                {
                    //Находим какая это по счету цифра - итератор + смещение на 1(из-за начала отсчета с 0) 
                    digitPlace = i+ NEXTVALUE + 1; 
                    digitForChange = numbers[i + NEXTVALUE];
                    //Определяем неизменяемую часть списка
                    if ((numbers.Count - digitPlace) > 0)
                    {
                        resultList.AddRange(numbers.GetRange(digitPlace, numbers.Count - digitPlace));
                        resultList.Reverse();
                    }
                    //Оставшиеся цифры
                    List<int> listForSorting = numbers.GetRange(0, digitPlace - 1);
                    //Находим элемент для обмена
                    int indexForMove = listForSorting.FindIndex(x=>x==listForSorting.Where(x => x > digitForChange).FirstOrDefault());
                    if (!(listForSorting[indexForMove] == 0 && digitPlace == numbers.Count))
                    {
                        //Заполняем результат цифрами, которые остались
                        resultList.Add(listForSorting[indexForMove]);
                        listForSorting[indexForMove] = digitForChange;
                        listForSorting.OrderBy(x => x);
                        resultList.AddRange(listForSorting);
                    }
                    else
                        return null;
                    return resultList;
                }
            }
           
            return null;
        }

        /// <summary>
        /// Разбор числа на список цифр
        /// </summary>
        /// <param name="value">Число введенное пользователем</param>
        /// <returns>Список цифр типа int</returns>
        static List<int> GetDigits(int value)
        {
            List<int> result = new List<int>();
            while (value != 0)
            {
                result.Add((value % 10));
                value /= 10;
            }
            
            return result;
        }



        static void Main(string[] args)
        {
            List<int> digits = new List<int>();
            Console.WriteLine("Please input your number:");
            string userNumber = Console.ReadLine();            
            int number = int.TryParse(userNumber, out number)?number:0;

            digits = GetDigits(number);
            List<int> result = GetNextNumber(digits);

            if (number > 0 && result != null)
            {                
                    Console.WriteLine(string.Join("", result));
            }
            else
                Console.WriteLine("-1");


            Console.WriteLine();            
        }
    }
}
